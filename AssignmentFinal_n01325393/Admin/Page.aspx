﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" Inherits="AssignmentFinal_n01325393.Admin.Page" CodeBehind="Page.aspx.cs" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    
     <asp:SqlDataSource runat="server" 
                        id="select_page"
                        ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource runat="server" 
                        id="delete_page"
                        ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    
    <div runat="server" id="status"></div>
    
    <div runat="server" id="pageInformation">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h1 runat="server" id="pageTitle" class="h3 mb-0">Page</h1>
            <div>
                <asp:Button runat="server" 
                            CssClass="btn btn-danger" 
                            id="deletePage"
                            OnClick="DeletePage"
                            OnClientClick="if(!confirm('Are you sure you want to delete this page?')) return false;"
                            Text="Delete" />
                <span runat="server" id="pageActions"></span>
            </div>
        </div>
    
        <table class="infoTable">
            <tr>
                <th>Created by:</th>
                <td runat="server" id="pageAuthor"></td>
            </tr>
            <tr>
                <th>Date created:</th>
                <td runat="server" id="pageDate"></td>
            </tr>
            <tr>
                <th>Status:</th>
                <td runat="server" id="pagePublished"></td>
            </tr>
        </table>
        
        <section class="row mt-5">
            <div class="col-md-8">
                <h2 class="h4">Content</h2>
                <div runat="server" id="pageContent"></div>
            </div>
            <div class="col-md-4">
                <h2 class="h4">Sidebar</h2>
                <div runat="server" id="pageSidebar"></div>
            </div>
        </section>
    </div>
    
</asp:Content>