﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" Inherits="AssignmentFinal_n01325393.Admin.AddPage" CodeBehind="AddPage.aspx.cs" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="d-flex justify-content-between align-items-center mb-4">
        <h1 class="h3 mb-0">Add Page</h1>
        <a class="btn btn-dark" href="Pages.aspx">Back</a>
    </div>
    <div>
        <asp:SqlDataSource runat="server" 
                           id="add_page"
                           ConnectionString="<%$ ConnectionStrings:db_connection %>">
        </asp:SqlDataSource>
        
        <div runat="server" id="status"></div>
    
        <div class="form-group">
            <label for="pageTitle">Title</label>
            <asp:TextBox runat="server" CssClass="form-control form-control-lg mb-1" ID="pageTitle" placeholder="e.g. Home"></asp:TextBox>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="authorNameRequiredValid" controltovalidate="pageTitle" errormessage="Please enter a title" />
            <asp:RegularExpressionValidator Display="Dynamic" ID="authorNameRegExValid" runat="server" ValidationExpression="[a-z|A-Z|0-9|\s]*" ControlToValidate="pageTitle" ErrorMessage="Enter a valid name. Use only latin characters, numbers and spaces."></asp:RegularExpressionValidator>
        </div> 
        
        <div class="form-row">
        
            <div class="form-group col-md-6">
                <label for="pageAuthor">Author</label>
                <uctrl:AdminAuthorPicker runat="server" id="pageAuthor"></uctrl:AdminAuthorPicker>
            </div>
            
            <div class="form-group col-md-6 pl-md-3">
                <label for="pagePublished">Publish Page?</label>
                <div class="formInlineList">
                    <asp:RadioButton runat="server" CssClass="form-check form-check-inline mr-4" Checked="true" ID="pagePublishedTrue" GroupName="pagePublished" Text="Yes" />
                    <asp:RadioButton runat="server" CssClass="form-check form-check-inline" ID="pagePublishedFalse" GroupName="pagePublished" Text="No" />
                </div>
            </div>
            
        </div>
        
        <div class="form-row mt-4">
        
            <div class="form-group col-md-8">
                <label for="pageContent">Content</label>
                <asp:TextBox runat="server" CssClass="form-control form-control mb-1" ID="pageContent" placeholder="Main Page Content" TextMode="MultiLine" Rows="20"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="pageContentRequiredValid" controltovalidate="pageContent" errormessage="Page has to have some content" />
            </div>
            
            <div class="form-group col-md-4">
                <label for="pageSidebar">Sidebar</label>
                <asp:TextBox runat="server" CssClass="form-control form-control mb-1" ID="pageSidebar" placeholder="Content for Sidebar" TextMode="MultiLine" Rows="20"></asp:TextBox>
            </div>
            
        </div>
        
        <div class="text-right">
            <asp:Button ID="pageSubmit" cssClass="btn btn-primary" Text="Add Page" OnClick="AddPageEntry" runat="server" />
        </div>
        
    </div>
</asp:Content>
