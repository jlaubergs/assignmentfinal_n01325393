﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" Inherits="AssignmentFinal_n01325393.Admin.EditAuthor" CodeBehind="EditAuthor.aspx.cs" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    
    <asp:SqlDataSource runat="server" 
                       id="edit_author"
                       ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource runat="server" 
                       id="select_author"
                       ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    
    <div runat="server" id="status"></div>
    
    <div runat="server" id="authorForm">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h1 class="h3 mb-0">Edit Author</h1>
            <a class="btn btn-dark" href="Authors.aspx">Back to all Authors</a>
        </div>
    
        <div class="form-group">
            <label for="authorName">Name</label>
            <asp:TextBox runat="server" CssClass="form-control form-control-lg mb-1" ID="authorName" placeholder="e.g. John Smith"></asp:TextBox>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="authorNameRequiredValid" controltovalidate="authorName" errormessage="Please enter authors name" />
            <asp:RegularExpressionValidator Display="Dynamic" ID="authorNameRegExValid" runat="server" ValidationExpression="^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$" ControlToValidate="authorName" ErrorMessage="Enter a valid name. Use only latin characters and spaces."></asp:RegularExpressionValidator>
        </div> 
        
        <div class="text-right">
            <asp:Button ID="authorSubmit" cssClass="btn btn-primary" Text="Save" OnClick="UpdateAuthor" runat="server" />
        </div>
    </div>
</asp:Content>
