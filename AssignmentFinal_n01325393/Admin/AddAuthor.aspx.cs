﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Globalization;

namespace AssignmentFinal_n01325393.Admin
{

    public partial class AddAuthor : System.Web.UI.Page
    {
        private string sqlline = "INSERT INTO Authors " +
                                 "(Name) " +
                                 "VALUES ";
 

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    
        public void AddAuthorEntry (object sender, EventArgs args)
        {

            // DEFINING VARIABLES
            string author_name = authorName.Text.ToString();
            
            sqlline += "('" + author_name + "')";
            
            add_author.InsertCommand = sqlline;
            add_author.Insert();

            // MESSAGE AT THE END OF THE FUNCTION
            // There's missing check whether the entry actually got added!
            status.InnerHtml = "<div class=\"alert alert-success\">Author: " + author_name + " has been sucessfully added!</div>";

        }
    }
    
}
