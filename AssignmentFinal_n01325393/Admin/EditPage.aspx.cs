﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace AssignmentFinal_n01325393.Admin
{

    public partial class EditPage : System.Web.UI.Page
    {
        
        public int PageID
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pageInfo = SelectPage(PageID);
            
            if (pageInfo == null)
            {
                pageForm.InnerHtml = "";
                status.InnerHtml = "<div class=\"alert alert-danger\">No Page Found <a href=\"Pages.aspx\">See All Pages</a></div>";
                return;
            }
            
            pageTitle.Text = pageInfo["Title"].ToString();
            
            pageContent.Text = pageInfo["ContentMain"].ToString();
            
            pageSidebar.Text = pageInfo["ContentSidebar"].ToString();

            string isPublished = pageInfo["IsPublished"].ToString();
            if ( isPublished == "1" ){
                pagePublishedTrue.Checked = true;
                pagePublishedFalse.Checked = false;
            } else {
                pagePublishedTrue.Checked = false;
                pagePublishedFalse.Checked = true;
            }
            

        }
        
        protected DataRowView SelectPage(int id)
        {
            string query = "SELECT * " +
                           "FROM Pages " +
                           "WHERE PageID=" + id;  
                           
            select_page.SelectCommand = query;

            DataView pageView = (DataView)select_page.Select(DataSourceSelectArguments.Empty);
            
            // Returns null if no result is found
            if (pageView.ToTable().Rows.Count < 1)
            {
                return null;
            }
            
            // Gets the first result
            DataRowView pageInfo = pageView[0]; 
            return pageInfo;

        }
        
        protected void UpdatePage(object sender, EventArgs e)
        {
        
            string page_title = pageTitle.Text.ToString();

            string page_author = "null";
            if( pageAuthor._selected_id != 0)
            {
                page_author = pageAuthor._selected_id.ToString();
            }

            string page_published = "";
            bool isPublished = pagePublishedTrue.Checked;
            page_published = isPublished ? "1" : "0";

            string page_content = pageContent.Text.ToString();
            string page_sidebar = pageSidebar.Text.ToString();
            

            string query = "UPDATE Pages " + 
                           "SET Title='" + page_title + "', " + 
                                "AuthorID=" + page_author + ", " + 
                                "IsPublished='" + page_published + "', " + 
                                "ContentMain='" + page_content + "', " +
                                "ContentSidebar='" + page_sidebar + "' " +
                           "WHERE PageID=" + PageID;

            //status.InnerHtml = query;
            edit_page.UpdateCommand = query;
            edit_page.Update();
            
            status.InnerHtml = "<div class=\"alert alert-success\">Page has been sucessfully updated!</div>";

        }
       

    }
}
