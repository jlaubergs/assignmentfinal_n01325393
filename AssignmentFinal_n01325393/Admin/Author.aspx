﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" Inherits="AssignmentFinal_n01325393.Admin.Author" CodeBehind="Author.aspx.cs" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <asp:SqlDataSource runat="server" 
                        id="select_author"
                        ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource runat="server" 
                        id="delete_author"
                        ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    
    <div runat="server" id="status"></div>
    
    <div runat="server" id="authorInformation">
        <div class="d-flex justify-content-between align-items-center mb-4">
            <h1 runat="server" id="authorName" class="h3 mb-0">Author</h1>
            <div>
                <asp:Button runat="server" 
                            CssClass="btn btn-danger" 
                            id="deleteAuthor"
                            OnClick="DeleteAuthor"
                            OnClientClick="if(!confirm('Are you sure you want to delete this author?')) return false;"
                            Text="Delete" />
                <span runat="server" id="authorActions"></span>
            </div>
        </div>
    
        <table class="infoTable">
            <tr>
                <th>Pages created:</th>
                <td runat="server" id="authorPageCount"></td>
            </tr>
        </table>
        
        <section runat="server" id="authorPages" class="mt-5">
        
            <h2 class="h4 mb-4">Author's Pages</h2>
            <!-- Dynamically adding user control -->
        </section>
        
    </div>
</asp:Content>
