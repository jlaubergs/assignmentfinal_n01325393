﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace AssignmentFinal_n01325393.Admin
{

    public partial class EditAuthor : System.Web.UI.Page
    {
    
        public int AuthorID
        {
            get { return Convert.ToInt32(Request.QueryString["authorid"]); }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView authorInfo = selectAuthor(AuthorID);
            
            if (authorInfo == null)
            {
                authorForm.InnerHtml = "";
                status.InnerHtml = "<div class=\"alert alert-danger\">No Author Found <a href=\"Authors.aspx\">See All Authors</a></div>";
                return;
            }
            
            authorName.Text = authorInfo["Name"].ToString();
        }
        
        protected DataRowView selectAuthor(int id)
        {
            string query = "SELECT * " +
                           "FROM Authors " +
                           "WHERE AuthorID=" + id;  
                           
            select_author.SelectCommand = query;

            DataView pageView = (DataView)select_author.Select(DataSourceSelectArguments.Empty);
            
            // Returns null if no result is found
            if (pageView.ToTable().Rows.Count < 1)
            {
                return null;
            }
            
            // Gets the first result
            DataRowView authorInfo = pageView[0]; 
            return authorInfo;

        }
        
        protected void UpdateAuthor(object sender, EventArgs e)
        {
        
            string author_name = authorName.Text.ToString();
           
            string query = "UPDATE Authors " + 
                           "SET Name='" + author_name + "' " + 
                           "WHERE AuthorID=" + AuthorID;
            
            
            edit_author.UpdateCommand = query;
            edit_author.Update();
            
            status.InnerHtml = "<div class=\"alert alert-success\">Author has been sucessfully updated!</div>";

        }

    }
}
