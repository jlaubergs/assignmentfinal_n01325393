﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="d-flex justify-content-between align-items-center mb-4">
        <h1 class="h3 mb-0">Authors</h1>
        <a class="btn btn-primary" href="AddAuthor.aspx">Add Author</a>
    </div>
    <div>
        <uctrl:AdminAuthorList runat="server"></uctrl:AdminAuthorList>
    </div>
</asp:Content>