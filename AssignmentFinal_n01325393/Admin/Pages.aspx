﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="d-flex justify-content-between align-items-center mb-4">
        <h1 class="h3 mb-0">Pages</h1>
        <a class="btn btn-primary" href="AddPage.aspx">Add Page</a>
    </div>
    <div>
        <uctrl:AdminPageList runat="server"></uctrl:AdminPageList>
    </div>
</asp:Content>