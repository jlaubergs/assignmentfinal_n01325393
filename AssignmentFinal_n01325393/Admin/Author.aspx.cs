﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace AssignmentFinal_n01325393.Admin
{

    public partial class Author : System.Web.UI.Page
    {
        
        public int AuthorID
        {
            get { return Convert.ToInt32(Request.QueryString["authorid"]); }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView authorInfo = SelectPage(AuthorID);
            
            if (authorInfo == null)
            {
                authorInformation.InnerHtml = "";
                status.InnerHtml = "<div class=\"alert alert-danger\">No Author Found <a href=\"Authors.aspx\">See All Authors</a></div>";
                return;
            }
            
            authorName.InnerHtml = "<span class=\"text-muted\">Author:</span> " + authorInfo["Name"].ToString();
            
            authorPageCount.InnerHtml = authorInfo["PageCount"].ToString();  

            authorActions.InnerHtml += "<a href=\"EditAuthor.aspx?authorid=" + authorInfo["AuthorID"].ToString() + "\" class=\"btn btn-dark ml-2\">Edit</a>";

            
            UserControls.AdminPageList uc = (UserControls.AdminPageList)LoadControl("~/UserControls/AdminPageList.ascx");
            uc.ManualAuthorID = AuthorID.ToString();
            authorPages.Controls.Add(uc);
         
            
        }
        
        protected DataRowView SelectPage(int id)
        {
            string query = "SELECT Authors.AuthorID, Authors.Name, COUNT(Pages.PageID) as PageCount " + 
                           "FROM Authors " + 
                           "LEFT JOIN Pages ON Authors.AuthorID = Pages.AuthorID " + 
                           "WHERE Authors.AuthorID=" + id + " " +
                           "GROUP BY Authors.AuthorID, Authors.Name";
                           
            select_author.SelectCommand = query;

            DataView pageView = (DataView)select_author.Select(DataSourceSelectArguments.Empty);
            
            // Returns null if no result is found
            if (pageView.ToTable().Rows.Count < 1)
            {
                return null;
            }
            
            // Gets the first result
            DataRowView authorInfo = pageView[0]; 
            
            return authorInfo;

        }
        
        protected void DeleteAuthor(object sender, EventArgs e)
        {   
        
            // Remove AuthorID from all pages
            string query = "UPDATE Pages " + 
                           "SET AuthorID = null " + 
                           "WHERE AuthorID=" + AuthorID;
       
            delete_author.UpdateCommand = query;
            delete_author.Update();
            
            // Remove Author

            query = "DELETE FROM Authors " +
                    "WHERE authorid=" + AuthorID;

            delete_author.DeleteCommand = query;
            delete_author.Delete();

        }
    

    }
}
