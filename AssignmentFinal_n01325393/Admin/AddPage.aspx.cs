﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Globalization;

namespace AssignmentFinal_n01325393.Admin
{

    public partial class AddPage : System.Web.UI.Page
    {
    
        private string sqlline = "INSERT INTO Pages " +
                                 "(Title, AuthorID, DatePublished, IsPublished, ContentMain, ContentSidebar) " +
                                 "VALUES ";
 

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    
        public void AddPageEntry (object sender, EventArgs args)
        {

            // DEFINING VARIABLES
            string page_title = pageTitle.Text.ToString();
            
            int page_author = pageAuthor._selected_id;
            
            string page_date = DateTime.Now.ToString("MM-dd-yyyy");
            
            string page_published = "";
            bool isPublished = pagePublishedTrue.Checked;
            page_published = isPublished ? "1" : "0";

            string page_content = pageContent.Text.ToString();
            string page_sidebar = pageSidebar.Text.ToString();
            
            // END - DEFINING VARIABLES


            //status.InnerHtml = page_author.ToString();
            
            sqlline += "('" + page_title + "', '" + page_author + "', '" + page_date + "', '" + page_published + "', '" + page_content + "', '" + page_sidebar + "')";
            
            add_page.InsertCommand = sqlline;
            add_page.Insert();

            // MESSAGE AT THE END OF THE FUNCTION
            // There's missing check whether the entry actually got added!
            status.InnerHtml = "<div class=\"alert alert-success\">Page: " + page_title + " has been sucessfully created!</div>";

        }
        
    }
}
