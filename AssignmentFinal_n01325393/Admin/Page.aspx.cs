﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace AssignmentFinal_n01325393.Admin
{

    public partial class Page : System.Web.UI.Page
    {

        public int PageID
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pageInfo = SelectPage(PageID);
            
            if (pageInfo == null)
            {
                pageInformation.InnerHtml = "";
                status.InnerHtml = "<div class=\"alert alert-danger\">No Page Found <a href=\"Pages.aspx\">See All Pages</a></div>";
                return;
            }
            
            pageTitle.InnerHtml = "<span class=\"text-muted\">Page:</span> " + pageInfo["Title"].ToString();
            
            pageAuthor.InnerHtml = "<a href=\"Author.aspx?authorid=" + pageInfo["AuthorID"].ToString() + "\">" + pageInfo["Author"].ToString() + "</a>";
            
            pageDate.InnerHtml = pageInfo["Date"].ToString();
            
            pageContent.InnerHtml = pageInfo["ContentMain"].ToString();
            
            pageSidebar.InnerHtml = pageInfo["ContentSidebar"].ToString();

            string isPublished = pageInfo["IsPublished"].ToString();
            if ( isPublished == "1" ){
                pagePublished.InnerHtml = "Published";
            } else {
                pagePublished.InnerHtml = "Not Published";
            }

            pageActions.InnerHtml += "<a href=\"EditPage.aspx?pageid=" + pageInfo["PageID"].ToString() + "\" class=\"btn btn-dark ml-2\">Edit</a>";
            

        }
        
        protected DataRowView SelectPage(int id)
        {
            string query = "SELECT PageID, Title, Authors.AuthorID, Authors.Name as Author, CONVERT(varchar(10), DatePublished, 103) as Date, IsPublished, ContentMain, ContentSidebar " +
                           "FROM pages " + 
                           "LEFT JOIN Authors ON Pages.AuthorID = Authors.AuthorID " +
                           "WHERE PageID=" + id;
                           
            select_page.SelectCommand = query;

            DataView pageView = (DataView)select_page.Select(DataSourceSelectArguments.Empty);
            
            // Returns null if no result is found
            if (pageView.ToTable().Rows.Count < 1)
            {
                return null;
            }
            
            // Gets the first result
            DataRowView pageInfo = pageView[0]; 
            
            return pageInfo;

        }
        
        protected void DeletePage(object sender, EventArgs e)
        {

            string query = "DELETE FROM Pages " +
                           "WHERE pageid=" + PageID;

            delete_page.DeleteCommand = query;
            delete_page.Delete();

        }

    }
}
