﻿<%@ Page Language="C#" MasterPageFile="~/Site_Admin.master" AutoEventWireup="true" Inherits="AssignmentFinal_n01325393.Admin.AddAuthor" CodeBehind="AddAuthor.aspx.cs" %>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="d-flex justify-content-between align-items-center mb-4">
        <h1 class="h3 mb-0">Add Author</h1>
        <a class="btn btn-dark" href="Authors.aspx">Back</a>
    </div>
    <div>
        <asp:SqlDataSource runat="server" 
                           id="add_author"
                           ConnectionString="<%$ ConnectionStrings:db_connection %>">
        </asp:SqlDataSource>
        
        <div runat="server" id="status"></div>
        
        <div class="form-group">
            <label for="authorName">Name</label>
            <asp:TextBox runat="server" CssClass="form-control form-control-lg mb-1" ID="authorName" placeholder="e.g. John Smith"></asp:TextBox>
            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="authorNameRequiredValid" controltovalidate="authorName" errormessage="Please enter authors name" />
            <asp:RegularExpressionValidator Display="Dynamic" ID="authorNameRegExValid" runat="server" ValidationExpression="^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$" ControlToValidate="authorName" ErrorMessage="Enter a valid name. Use only latin characters and spaces."></asp:RegularExpressionValidator>
        </div> 
        
        <div class="text-right">
            <asp:Button ID="authorSubmit" cssClass="btn btn-primary" Text="Add Author" OnClick="AddAuthorEntry" runat="server" />
        </div>
        
    </div>
</asp:Content>
