﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentFinal_n01325393.UserControls
{

    public partial class PageNav : System.Web.UI.UserControl
    {

        private string sqlline = "SELECT PageID, Title " +
                                 "FROM Pages " +
                                 "WHERE IsPublished=1";
                                 
        protected void Page_Load(object sender, EventArgs e)
        {
        
            pages_select.SelectCommand = sqlline;
            RenderNav(pages_select);
            
        }
        
        void RenderNav(SqlDataSource src)
        {

            string navHTML = "";
            DataView pagesListView = (DataView)src.Select(DataSourceSelectArguments.Empty);
            
            foreach (DataRowView row in pagesListView)
            {
                navHTML += "<li class=\"nav-item\"><a class=\"nav-link\" href=\"/Page.aspx?pageid=" + row["PageID"] + "\">" + row["Title"] + "</a></li>";
            }

            pageMenu.InnerHtml = navHTML;
            
        }
                                 
        
            
    }
}
