﻿<%@ Control Language="C#" Inherits="AssignmentFinal_n01325393.UserControls.AdminAuthorList" CodeBehind="AdminAuthorList.ascx.cs" %>

<div runat="server" id="status"></div>

<asp:SqlDataSource runat="server"
                   id="authors_list_select"
                   ConnectionString="<%$ ConnectionStrings:db_connection %>">
</asp:SqlDataSource>

<asp:SqlDataSource runat="server"
                   id="author_delete"
                   ConnectionString="<%$ ConnectionStrings:db_connection %>">
</asp:SqlDataSource>

<asp:DataGrid runat="server"
              id="authors_list"           
              AutoGenerateColumns="False"
              CssClass="dataTable"
              OnItemCommand="AuthorAction">
</asp:DataGrid>

<div runat="server" id="data"></div>