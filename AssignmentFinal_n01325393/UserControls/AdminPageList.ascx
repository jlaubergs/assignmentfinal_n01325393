﻿<%@ Control Language="C#" Inherits="AssignmentFinal_n01325393.UserControls.AdminPageList" CodeBehind="AdminPageList.ascx.cs" %>

<div runat="server" id="status"></div>

<asp:SqlDataSource runat="server"
                   id="pages_list_select"
                   ConnectionString="<%$ ConnectionStrings:db_connection %>">
</asp:SqlDataSource>

<asp:SqlDataSource runat="server"
                   id="page_delete"
                   ConnectionString="<%$ ConnectionStrings:db_connection %>">
</asp:SqlDataSource>

<asp:DataGrid runat="server"
              id="pages_list"           
              AutoGenerateColumns="False"
              CssClass="dataTable"
              OnItemCommand="PageAction">
</asp:DataGrid>

<div runat="server" id="data"></div>