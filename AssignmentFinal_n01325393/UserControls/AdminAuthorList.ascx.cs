﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentFinal_n01325393.UserControls
{

    public partial class AdminAuthorList : System.Web.UI.UserControl
    {
    
        private string sqlline = "SELECT Authors.AuthorID, Authors.Name, COUNT(Pages.PageID) as PageCount " + 
                                 "FROM Authors " + 
                                 "LEFT JOIN Pages ON Authors.AuthorID = Pages.AuthorID " +
                                 "GROUP BY Authors.AuthorID, Authors.Name";
    
        protected void Page_Load(object sender, EventArgs e)
        {
        
            authors_list_select.SelectCommand = sqlline;               
            authors_list.DataSource = RenderData(authors_list_select);
            
            // DEFINING COLUMNS
            
            BoundColumn col_id = new BoundColumn();
            col_id.DataField = "AuthorID";
            col_id.HeaderText = "ID";
            col_id.Visible = false;
            authors_list.Columns.Add(col_id);
            
            BoundColumn col_author = new BoundColumn();
            col_author.DataField = "Name";
            col_author.HeaderText = "Name";
            authors_list.Columns.Add(col_author);
            
            BoundColumn col_pagecount = new BoundColumn();
            col_pagecount.DataField = "PageCount";
            col_pagecount.HeaderText = "Pages Created";
            authors_list.Columns.Add(col_pagecount);
            
            BoundColumn col_edit = new BoundColumn();
            col_edit.DataField = "Edit";
            col_edit.HeaderText = "Edit";
            authors_list.Columns.Add(col_edit);
            
            // CREATING NEW BUTTON COLUMN
            ButtonColumn col_delete = new ButtonColumn();
            col_delete.HeaderText = "Delete";
            col_delete.Text = "Delete";
            col_delete.ButtonType = ButtonColumnType.PushButton;
            col_delete.CommandName = "DeleteAuthor";
            authors_list.Columns.Add(col_delete);
            
            // END - DEFINING COLUMNS
            
            authors_list.DataBind();
            
        }

        protected DataView RenderData(SqlDataSource src)
        { 
          
            DataTable renderedTable;  
            DataView renderedData;
            
            renderedTable = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            
            // ADD NEW COLUMN
            DataColumn col_edit = new DataColumn();
            col_edit.ColumnName = "Edit";
            renderedTable.Columns.Add(col_edit);
            // END - ADD NEW COLUMN
            
            
            // MOFIDY COLUMNS
            foreach (DataRow row in renderedTable.Rows)
            {

                row["Name"] = "<a href=\"Author.aspx?authorid=" + row["AuthorID"] + "\">" + row["Name"] + "</a>";
                row[col_edit] = "<a href=\"EditAuthor.aspx?authorid=" + row["AuthorID"] + "\">Edit</a>";
                               
            }
            // END - MOFIDY COLUMNS
            
            renderedData = renderedTable.DefaultView;
            return renderedData;
        
        }
        
        protected void AuthorAction(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName=="DeleteAuthor")
            {
                // Gets the ID from the first hidden collumn
                string author_id = Convert.ToInt32(e.Item.Cells[0].Text).ToString();
                DeleteAuthor(author_id);
            }

        }
        
        protected void DeleteAuthor(string authorid)
        {   
            
            // Remove AuthorID from all pages
            string query = "UPDATE Pages " + 
                           "SET AuthorID = null " + 
                           "WHERE AuthorID=" + authorid;
       
            author_delete.UpdateCommand = query;
            author_delete.Update();
            
            // Remove author
            query = "DELETE FROM Authors WHERE AuthorID=" + authorid;
            author_delete.DeleteCommand = query;
            author_delete.Delete();
            
            status.InnerHtml = "<div class=\"alert alert-success\">Author with ID " + authorid + " has been sucessfully deleted!</div>";
        }
    
    }
}
