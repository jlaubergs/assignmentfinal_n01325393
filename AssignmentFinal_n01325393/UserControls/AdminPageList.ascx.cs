﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentFinal_n01325393.UserControls
{

    public partial class AdminPageList : System.Web.UI.UserControl
    {
        
        public string ManualAuthorID { get; set; }
       
        
        
        private string sqlline = "SELECT PageID, Title, Authors.AuthorID, Authors.Name as Author, CONVERT(varchar(10), DatePublished, 103) as Date, IsPublished as Published " + 
                                 "FROM pages " + 
                                 "LEFT JOIN Authors ON Pages.AuthorID = Authors.AuthorID";
        
    
        protected void Page_Load(object sender, EventArgs e)
        {
               
            if ( ManualAuthorID != null  )
            {
                sqlline += " WHERE Authors.AuthorID=" + this.ManualAuthorID;
            }

            pages_list_select.SelectCommand = sqlline;               
            pages_list.DataSource = RenderData(pages_list_select);
            
            // DEFINING COLUMNS
            
            // Need it in order to have a column that can be referenced
            // for actions like Delete
            BoundColumn col_id = new BoundColumn();
            col_id.DataField = "PageID";
            col_id.HeaderText = "ID";
            col_id.Visible = false;
            pages_list.Columns.Add(col_id);
            
            BoundColumn col_title = new BoundColumn();
            col_title.DataField = "Title";
            col_title.HeaderText = "Title";
            pages_list.Columns.Add(col_title);
            
            BoundColumn col_author = new BoundColumn();
            col_author.DataField = "Author";
            col_author.HeaderText = "Author";
            pages_list.Columns.Add(col_author);
            
            BoundColumn col_created = new BoundColumn();
            col_created.DataField = "Date";
            col_created.HeaderText = "Created On";
            pages_list.Columns.Add(col_created);
            
            BoundColumn col_published = new BoundColumn();
            col_published.DataField = "Published";
            col_published.HeaderText = "Is published?";
            pages_list.Columns.Add(col_published);
            
            BoundColumn col_edit = new BoundColumn();
            col_edit.DataField = "Edit";
            col_edit.HeaderText = "Edit";
            pages_list.Columns.Add(col_edit);
            
            // CREATING NEW BUTTON COLUMN
            ButtonColumn col_delete = new ButtonColumn();
            col_delete.HeaderText = "Delete";
            col_delete.Text = "Delete";
            col_delete.ButtonType = ButtonColumnType.PushButton;
            col_delete.CommandName = "DeletePage";
            pages_list.Columns.Add(col_delete);
            
            // END - DEFINING COLUMNS
            
            pages_list.DataBind();
            
        }

        protected DataView RenderData(SqlDataSource src)
        { 
          
            DataTable renderedTable;  
            DataView renderedData;
            
            renderedTable = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            
            // ADD NEW COLUMN
            DataColumn col_edit = new DataColumn();
            col_edit.ColumnName = "Edit";
            renderedTable.Columns.Add(col_edit);
            // END - ADD NEW COLUMN
            
            
            // MOFIDY COLUMNS
            foreach (DataRow row in renderedTable.Rows)
            {

                row["Title"] = "<a href=\"Page.aspx?pageid=" + row["PageID"] + "\">" + row["Title"] + "</a>";
                row["Author"] = "<a href=\"Author.aspx?authorid=" + row["AuthorID"] + "\">" + row["Author"] + "</a>";
                row["Published"] = Equals(row["Published"], "1") ? "Yes" : "No";
                row[col_edit] = "<a href=\"EditPage.aspx?pageid=" + row["PageID"] + "\">Edit</a>";
                               
            }
            // END - MOFIDY COLUMNS
            
            renderedData = renderedTable.DefaultView;
            return renderedData;
        
        }
        
        protected void PageAction(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName=="DeletePage")
            {
                // Gets the ID from the first hidden collumn
                string page_id = Convert.ToInt32(e.Item.Cells[0].Text).ToString();
                DeletePage(page_id);
            }

        }
        
        protected void DeletePage(string pageid)
        {
            string query = "DELETE FROM Pages WHERE PageID=" + pageid;
            page_delete.DeleteCommand = query;
            page_delete.Delete();
            
            status.InnerHtml = "<div class=\"alert alert-success\">Page with ID " + pageid + " has been sucessfully deleted!</div>";
        }
        
    
    }
}
