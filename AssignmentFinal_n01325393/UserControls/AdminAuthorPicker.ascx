﻿<%@ Control Language="C#" Inherits="AssignmentFinal_n01325393.UserControls.AdminAuthorPicker" CodeBehind="AdminAuthorPicker.ascx.cs" %>

<asp:SqlDataSource runat="server"
                   id="authors_list_select"
                   ConnectionString="<%$ ConnectionStrings:db_connection %>">
</asp:SqlDataSource>

<asp:DropDownList CssClass="form-control" 
                  id="author_picker" 
                  runat="server" 
                  OnSelectedIndexChanged="AssignAuthorID">
</asp:DropDownList>

<div runat="server" id="test"></div>