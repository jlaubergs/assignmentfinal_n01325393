﻿<%@ Control Language="C#" Inherits="AssignmentFinal_n01325393.UserControls.PageNav" CodeBehind="PageNav.ascx.cs" %>

<asp:SqlDataSource runat="server"
                   id="pages_select"
                   ConnectionString="<%$ ConnectionStrings:db_connection %>">
</asp:SqlDataSource>

<ul id="pageMenu" runat="server" class="nav navbar-nav mt-2 mt-md-0 mt-lg-0"></ul>