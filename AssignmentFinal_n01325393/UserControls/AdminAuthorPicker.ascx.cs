﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AssignmentFinal_n01325393.UserControls
{

    public partial class AdminAuthorPicker : System.Web.UI.UserControl
    {
    
        private string sqlline = "SELECT AuthorID, Name " + 
                                 "FROM Authors ";
        
        private int selected_id; // Actual field
        public int _selected_id  // Accessor
        {
            get { return selected_id; }
            set { selected_id = value; }
        }
        
        
       
        //////////////////////////////////////////////////////////////////////
       
        protected void Page_Load(object sender, EventArgs e)
        {
        
            authors_list_select.SelectCommand = sqlline;
            RenderPicker(authors_list_select, "author_picker");
            
        }
        
        void RenderPicker(SqlDataSource src, string controlID)
        { 
        
            DropDownList authorsList = (DropDownList)FindControl(controlID);
            authorsList.Items.Clear(); 
            DataView authorsListView = (DataView)src.Select(DataSourceSelectArguments.Empty);
            
            foreach (DataRowView row in authorsListView)
            {
                ListItem author_single = new ListItem();
                author_single.Text = row["Name"].ToString();
                author_single.Value = row["AuthorID"].ToString();
                authorsList.Items.Add(author_single);
                
            }
            
            authorsList.DataBind();
            
            if( author_picker.Items.Count >= 1)
            {
                _selected_id = int.Parse(author_picker.SelectedValue);
            }           

        }
        
        protected void AssignAuthorID(object sender, EventArgs e)
        {
            _selected_id = int.Parse(author_picker.SelectedValue);
        }
    
    }
}
