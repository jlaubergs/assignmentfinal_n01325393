﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="AssignmentFinal_n01325393.Page" CodeBehind="Page.aspx.cs" %>

<asp:Content ContentPlaceHolderID="title" runat="server">
    <h1 class="display-4" runat="server" id="pageTitle"></h1>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <asp:SqlDataSource runat="server" 
                       id="select_page"
                       ConnectionString="<%$ ConnectionStrings:db_connection %>">
    </asp:SqlDataSource>
    <div runat="server" id="pageContent"></div>
</asp:Content>

<asp:Content ContentPlaceHolderID="sidebar" runat="server">
    <div runat="server" id="pageSidebar"></div>
</asp:Content>