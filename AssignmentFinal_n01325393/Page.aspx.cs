﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace AssignmentFinal_n01325393
{

    public partial class Page : System.Web.UI.Page
    {
    
        public int pageID
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pageInfo = selectPage(pageID);
            
            if (pageInfo == null)
            {
                //status.InnerHtml = "<div class=\"alert alert-alert\">No Page Found</div>";
                return;
            }
            
            pageTitle.InnerHtml = pageInfo["Title"].ToString();
            
            pageContent.InnerHtml = pageInfo["ContentMain"].ToString();
            
            pageSidebar.InnerHtml = pageInfo["ContentSidebar"].ToString();

        }
        
        protected DataRowView selectPage(int id)
        {
            string query = "SELECT * " +
                           "FROM Pages " +
                           "WHERE PageID=" + pageID.ToString();  
                           
            select_page.SelectCommand = query;

            DataView pageView = (DataView)select_page.Select(DataSourceSelectArguments.Empty);
            
            // Returns null if no result is found
            if (pageView.ToTable().Rows.Count < 1)
            {
                return null;
            }
            
            // Gets the first result
            DataRowView pageInfo = pageView[0]; 
            return pageInfo;

        }

    }
}
